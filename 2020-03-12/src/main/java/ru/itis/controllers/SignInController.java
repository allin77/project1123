package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import ru.itis.dto.SignInDto;
import ru.itis.services.SignInService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

public class SignInController implements Controller {

    @Autowired
    private SignInService signInService;


    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getMethod().equals("GET")) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("sign-in");
            return modelAndView;
        } else if (request.getMethod().equals("POST")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            SignInDto form = SignInDto.builder()
                    .email(email)
                    .password(password)
                    .build();

            Optional<String> cookie = signInService.signIn(form);

            if (cookie.isPresent()) {
                request.getSession().setAttribute("AUTH", cookie.get());
                response.sendRedirect("/signUp");
            }
        } else {
            response.sendRedirect("/signIn");
        }
        throw new MethodNotAllowedException(HttpMethod.PUT, Arrays.asList(HttpMethod.GET, HttpMethod.POST));
    }

}
