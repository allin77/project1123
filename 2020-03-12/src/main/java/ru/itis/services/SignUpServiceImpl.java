package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.itis.dto.SignUpDto;
import ru.itis.models.Mail;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

import java.util.HashMap;
import java.util.Map;

@Component
public class SignUpServiceImpl implements SignUpService {
    @Autowired
    private MailService mailService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpDto form) {
        String rawPassword = form.getPassword();
        String hashPassword = passwordEncoder.encode(rawPassword);

        User user = User.builder()
                .email(form.getEmail())
                .name(form.getName())
                .hashPassword(hashPassword)
                .build();

        userRepository.save(user);

        Map<String, String> model = new HashMap<>();

        model.put("name", user.getName());
        model.put("link", "http://localhost:8080/confirm/" + user.getEmail());

        Mail mail = Mail.builder()
                .subject("Registration")
                .to(form.getEmail())
                .model(model)
                .from("musin.almaz33@gmail.com")
                .build();

        mailService.sendEmail(mail);

    }
}
