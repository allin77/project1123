package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import ru.itis.dto.SignUpDto;
import ru.itis.services.SignUpService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public class SignUpController implements Controller {

    @Autowired
    private SignUpService signUpService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getMethod().equals("GET")) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("sign-in");
            return modelAndView;
        } else if (request.getMethod().equals("POST")) {
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            SignUpDto form = SignUpDto.builder()
                    .email(email)
                    .name(name)
                    .password(password)
                    .build();

            signUpService.signUp(form);
            response.sendRedirect("/signIn");
        }

        throw new MethodNotAllowedException(HttpMethod.PUT, Arrays.asList(HttpMethod.GET, HttpMethod.POST));
    }
}
