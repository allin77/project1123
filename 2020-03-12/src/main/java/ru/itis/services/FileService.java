package ru.itis.services;

import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.FileInfo;

public interface FileService {
    FileInfo save(MultipartFile multipartFile);

    FileInfo get(String url);
}
