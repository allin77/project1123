package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.FileInfo;
import ru.itis.repositories.FileInfoRepository;

import java.util.Optional;
import java.util.UUID;

@Component
public class FileServiceImpl implements FileService {
    @Autowired
    private FileInfoRepository fileInfoRepository;


    @Override
    public FileInfo save(MultipartFile multipartFile) {
        UUID storageName = UUID.randomUUID();
        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(multipartFile.getOriginalFilename())
                .storageFileName(storageName.toString())
                .size(multipartFile.getSize())
                .type(multipartFile.getContentType())
                .url(UUID.randomUUID().toString())
                .build();

        fileInfoRepository.save(fileInfo);
        return fileInfo;
    }

    @Override
    public FileInfo get(String name) {
        Optional<FileInfo> fileInfoCandidate = fileInfoRepository.find(name);
        return fileInfoCandidate.orElse(null);
    }
}
